/* Codigo com a biblioteca convert.h
 *
 *
 *
 *
 * Autor: Nicolas Costa Stefani
 * Data: 02/12/2021
 */
#include <stdio.h>
#include <stdlib.h>
#include "convert.h"

int main(void)
{
    int n, escolha;

    printf("===============MENU===================\n\nconverter decimal para binario: 1\nconverter binario para decimal: 2\n\n");
    printf("digite sua escolha: ");
    scanf("%d", &escolha);

    switch(escolha)
    {
    case 1:

        printf("\ndigite um valor decimal: ");
        scanf("%d", &n);

        printf("\nO numero decimal %d convertido para binario eh: %ld", n, dec2bin(n));

        printf("\n");

        break;

    case 2:

        printf("\ndigite um valor em binario: ");
        scanf("%d", &n);

        printf("\nO numero binario %d convertido para decimal eh: %d", n, bin2dec(n));

        break;

    default:
        printf("\nopcao invalida!\n");
    }

    return 0;
}
