#ifndef CONVERT_H
#define CONVERT_H


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

long int dec2bin(int n){
    if(n == 0)
    {
        return 0;
    }
    else
    {
        return dec2bin(n/2)*10 + n%2;
    }
}

int bin2dec(int n){
    int result=0, i;

    for(i=0; n>0; i++)
    {
        result = result + pow(2,i)*(n%10);
        n = n/10;
    }

    return (result);
}

#endif
