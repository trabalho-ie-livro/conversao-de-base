# Algoritmo para Conversão de Base

## Descrição

Este algoritmo tem como função converter números decimais (1,2,3,4,5,6,7...) para números binarios (101, 1111001, 110110, etc) e vice-versa

### Como funciona o algoritmo

Para a conversão de números decimais para binarios é utilizado o método de divisões sucesssivas pelo número 2 até que o resultado seja igual a zero, após isso o resto da primeira divisão será o último algarismo do número binario e os resultados das divisões serão os outros algarismos do número binario.

![](https://calculareconverter.com.br/wp-content/uploads/2018/05/decimal-em-bin%C3%A1rio.png)

já para a conversão de binario para decimal é utilizado o método de potências onde o primeiro digito da direita para a esquerda é o expoente zero e vai aumentando de um em um quanto mais para a esquerda o número se extende, após isso o número 2 será elevado pela potência correspondente a posição digito análisado e após a potenciação sera multiplicado pelo algarismo do número binário que se deseja converter.

![](https://sites.google.com/site/maratonaufabc/_/rsrc/1472606260682/topicos/matematica/bases/bindec.png)


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -lm -o main
./main
```


## Autor
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168707/avatar.png?width=400)  | Nicolas Costa Stefani | [@nicolasstefani](https://gitlab.com/nicolasstefani) | [nicolasstefani@alunos.utfpr.edu.br](mailto:nicolasstefani@alunos.utfpr.edu.br)





