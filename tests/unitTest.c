#include <assert.h>
#include "convert.h"

void TEST_BIN2DEC(void)
{
  // test bin2dec
  int numTest = 101;
  assert(bin2dec(numTest) == 5);
}

int main(int argc, char* argv[])
{

  TEST_BIN2DEC();
  printf("Teste Binario para Decimal - Passou!\n");
  // ...

  return 0;
}
